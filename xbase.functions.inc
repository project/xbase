<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\druhels\EntityHelper;

/**
 * Return entity html classes.
 */
function xbase_get_entities_html_classes_map(): array {
  static $cache;

  if ($cache === NULL) {
    $theme_manager = \Drupal::service('theme.manager');
    $active_theme = $theme_manager->getActiveTheme();
    $cache = $active_theme->getExtension()->info['entity-html-classes'] ?? [];
  }

  return $cache;
}

/**
 * Return entity html class.
 */
function xbase_get_entity_html_class(EntityInterface $entity = NULL, string $entity_type = NULL, string $entity_bundle = NULL): string {
  static $cache = [];

  if ($entity) {
    if (!$entity_type) {
      $entity_type = $entity->getEntityTypeId();
    }
    if (!$entity_bundle) {
      $entity_bundle = $entity->bundle();
    }
  }

  $cache_key = $entity_type . ':' . $entity_bundle;

  if (!isset($cache[$cache_key])) {
    // Set default html class
    $entity_html_class = $entity_type;
    if ($entity_bundle != 'default' && $entity_bundle != $entity_type) {
      $entity_html_class = $entity_bundle . '-' . $entity_html_class;
    }

    // Rewrite html class based on theme setting "entity-html-classes"
    $entity_html_classes = xbase_get_entities_html_classes_map();
    if ($entity_html_classes && isset($entity_html_classes[$entity_type])) {
      if (isset($entity_html_classes[$entity_type][$entity_bundle])) {
        $entity_html_class = $entity_html_classes[$entity_type][$entity_bundle];
      }
      elseif (isset($entity_html_classes[$entity_type]['*'])) {
        $entity_html_class = $entity_html_classes[$entity_type]['*'];
      }
    }

    $cache[$cache_key] = Html::getClass($entity_html_class);
  }

  return $cache[$cache_key];
}

/**
 * Return field html class.
 */
function xbase_get_field_html_class(string $field_name, EntityInterface $entity = NULL, string $entity_type = NULL, string $entity_bundle = NULL): string {
  static $cache = [];

  if ($entity) {
    if (!$entity_type) {
      $entity_type = $entity->getEntityTypeId();
    }
    if (!$entity_bundle) {
      $entity_bundle = $entity->bundle();
    }
  }

  $cache_key = $field_name . ':' . $entity_type . ':' . $entity_bundle;

  if (!isset($cache[$cache_key])) {
    $entity_html_class = xbase_get_entity_html_class($entity, $entity_type, $entity_bundle);

    $remove_words = [
      'extra_field_' . $entity_type . '_',
      'extra_field_' . $entity_bundle . '_',
      'extra_field_' . $entity_html_class . '_',
      'extra_field_',
      'field_' . $entity_type . '_',
      'field_' . $entity_bundle . '_',
      'field_' . str_replace('_', '', $entity_bundle) . '_',
      'field_' . $entity_html_class . '_',
      'field_',
      $entity_type . '_',
    ];
    $field_html_class_suffix = str_replace($remove_words, '', $field_name);

    $cache[$cache_key] = ($entity_html_class && $field_html_class_suffix) ? $entity_html_class . '__' . Html::getClass($field_html_class_suffix) : 'field';
  }

  return $cache[$cache_key];
}

/**
 * Return block main html class.
 */
function xbase_get_block_html_class(array $vars): string {
  static $current_theme_name;

  if ($current_theme_name == NULL) {
    $current_theme_name = \Drupal::service('theme.manager')->getActiveTheme()->getName();
  }

  $block_html_id = $vars['elements']['#id'] ?? str_replace(':', '-', $vars['plugin_id']);
  $block_main_class = Html::getClass($block_html_id);

  // Remove prefix "themename-" and "block-" from main class
  $block_main_class = preg_replace('/^(' . $current_theme_name . '|block' . ')-/', '', $block_main_class);

  // Add suffix "-block" to main class
  if (!str_ends_with($block_main_class, '-block')) {
    $block_main_class .= '-block';
  }

  return $block_main_class;
}

/**
 * Return region main html class.
 */
function xbase_get_region_html_class(string $region_id): string {
  return Html::getClass($region_id) . '-region';
}

/**
 * Return entity machine name.
 */
function xbase_get_content_entity_machine_name(ContentEntityInterface $entity, string $field_name = 'field_machine_name'): ?string {
  return EntityHelper::getExistsFieldValue($entity, $field_name);
}

/**
 * Return cleaned suggestion.
 */
function xbase_clean_suggestion(string $suggestion): string {
  $suggestion = strtolower($suggestion);
  $suggestion = trim($suggestion, '_');
  return preg_replace('/[^a-z0-9_]/', '_', $suggestion);
}
