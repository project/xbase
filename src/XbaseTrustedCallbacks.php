<?php

namespace Drupal\xbase;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\druhels\DrupalHelper;

class XbaseTrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'breadcrumbPageTitleLazyBuilder',
      'viewsPreRender',
    ];
  }

  /**
   * Breadcrumb page title lazy builder.
   *
   * @see xbase_preprocess_breadcrumb()
   */
  public static function breadcrumbPageTitleLazyBuilder(): array {
    $title = _xbase_current_page_h1() ?: DrupalHelper::getCurrentPageTitle();
    return is_array($title)
      ? $title
      : ['#plain_text' => $title ? strip_tags($title) : ''];
  }

  /**
   * View element pre render callback.
   *
   * @see xbase_element_info_alter()
   */
  public static function viewsPreRender($element): array {
    if (!empty($element['#theme_wrappers'])) {
      $element['#theme_wrappers'] = array_diff($element['#theme_wrappers'], ['container']);
    }

    return $element;
  }

}
