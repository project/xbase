(function ($) {

  $.widget('ui.dialog', $.ui.dialog, {
    /**
     * Override _focusTabbable().
     */
    _focusTabbable: function () {
      // Focus to first autofocus element instead of first element
      var $autofocusElement = this.element.find('[autofocus]:first');
      if ($autofocusElement.length && !$autofocusElement.is(':focus')) {
        $autofocusElement.focus();
      }
      else {
        // Focus to dialog in order to work Esc button
        var _this = this;
        setTimeout(function () {
          _this.uiDialog.focus();
        }, 0);
      }
    },

    /**
     * Override _createButtons().
     */
    _createButtons: function () {
      // Fix bug when button in buttons array does not contain "click" property. Example:
      // buttons: [
      //   close: {
      //     text: 'Close'
      //   }
      // ]
      $.each(this.options.buttons, function(name, props) {
        if (!props.click) {
          props.click = $.noop;
        }
      });

      this._superApply(arguments);
    },

    /**
     * Override _title().
     */
    _title: function (title) {
      // Allow html in title
      if (this.options.title) {
        title.html(this.options.title);
      }
      else {
        title.html("&#160;");
      }
    },
  });

})(jQuery);
