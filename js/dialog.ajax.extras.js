(function (Drupal) {

  // Remove dialog buttons functionality
  Drupal.behaviors.dialog.prepareDialogButtons = function () {
    return [];
  };

  // Override openDialog command and add triggeredElement to dialog options
  var originalOpenDialogCommand = Drupal.AjaxCommands.prototype.openDialog;
  Drupal.AjaxCommands.prototype.openDialog = function (ajax, response, status) {
    response.dialogOptions.triggeredElement = ajax.element;
    originalOpenDialogCommand.apply(this, arguments);
  };

})(Drupal);
