/**
 * Example:
 *
 * <div class="my-block">
 *   <div class="my-block__content">...</div>
 * </div>
 *
 * <script>
 *   $('.my-block').showMore({
 *     contentElement: $('.my-block__content'),
 *   });
 * </script>
 *
 * <style>
 *   .my-block__content {
 *     max-height: 100px;
 *     overflow: hidden;
 *   }
 * </style>
 */
;(function($, window, document, undefined) {

  'use strict';

  var pluginName = 'showMore';
  var defaults = {
    contentElement: null,
    maxHeightElement: null,
    linkElement: null,
    linkTextOpen: Drupal.t('Open'),
    linkTextClose: Drupal.t('Close'),
    minDifference: 5,
  };

  function Plugin (element, options) {
    var plugin = this;
    plugin.element = element;
    plugin.settings = $.extend({}, defaults, options);
    plugin._defaults = defaults;
    plugin.init();
  }

  $.extend(Plugin.prototype, {
    /**
     * Init.
     */
    init: function () {
      var plugin = this;
      var $element = $(plugin.element);
      var settings = plugin.settings;

      $element.addClass('show-more');

      // Process content
      if (!settings.contentElement) {
        settings.contentElement = $element.find('.show-more__content');

        if (settings.contentElement.length == 0) {
          settings.contentElement = $('<div class="show-more__content" />');
          $element.wrapInner(settings.contentElement);
        }
      }
      else {
        settings.contentElement = $(settings.contentElement);
        settings.contentElement.addClass('show-more__content');
      }

      // Process link
      if (!settings.linkElement) {
        settings.linkElement = $element.find('.show-more__link');

        if (settings.linkElement.length == 0) {
          settings.linkElement = $('<a class="show-more__link" href="#">' + settings.linkTextOpen + '</a>');
          settings.linkElement.appendTo($element);
        }
      }
      else {
        settings.linkElement = $(settings.linkElement);
        settings.linkElement.addClass('show-more__link');
      }
      settings.linkTextOpen = settings.linkElement.text();

      if (!settings.maxHeightElement) {
        if (parseInt($element.css('max-height'), 10) > 1) {
          settings.maxHeightElement = $element;
        }
        else {
          settings.maxHeightElement = settings.contentElement;
        }
      }

      // Click handler
      settings.linkElement.on('click', function (event) {
        var $link = $(this);

        // Open
        if (!$element.hasClass('show-more--open')) {
          $element.addClass('show-more--open');
          $link.html(settings.linkTextClose);
        }
        // Close
        else {
          $element.removeClass('show-more--open');
          $link.html(settings.linkTextOpen);
        }

        event.preventDefault();
      });

      var resizeObserver = new ResizeObserver(function (entries) {
        plugin.refresh();
      });
      resizeObserver.observe(settings.maxHeightElement[0]);
    },

    /**
     * Refresh.
     */
    refresh: function () {
      var plugin = this;
      var $element = $(plugin.element);
      var settings = plugin.settings;

      $element.removeClass('show-more--active show-more--inactive');

      var maxHeight = parseInt(settings.maxHeightElement.css('max-height'), 10);
      var realHeight = settings.maxHeightElement[0].scrollHeight;

      if (maxHeight <= 1 || maxHeight + settings.minDifference >= realHeight) {
        $element.addClass('show-more--inactive');
      }
      else {
        $element.addClass('show-more--active');
      }
    },
  });

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, pluginName + 'Plugin')) {
        $.data(this, pluginName + 'Plugin', new Plugin(this, options));
      }
    });
  };

})(jQuery, window, document);
