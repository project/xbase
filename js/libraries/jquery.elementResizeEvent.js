;(function($) {

  'use strict';

  $.fn.elementResizeEvent = function (callback) {
    return this.each(function () {
      var element = this;
      var resizeObserver = new ResizeObserver(function (entries) {
        $.proxy(callback, element)();
      });
      resizeObserver.observe(element);
    });
  };

})(jQuery);
