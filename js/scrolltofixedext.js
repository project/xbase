/**
 * Addition for ScrollToFixed.
 */
;(function($, window, document, undefined) {
  'use strict';

  var pluginName = 'scrollToFixedExt';
  var defaults = {
    baseClassName: 'sticky',
    className: 'sticky',
    spacerClass: 'sticky-spacer',
    zIndex: '',
    addAnimationClass: false,
    dontAddStyles: false,
    startPosition: 0,
  };

  function Plugin(element, options) {
    this.element = element;
    this.settings = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginName;
    this.init();
  }

  $.extend(Plugin.prototype, {
    init: function() {
      var settings = this.settings;

      // New "startPosition" setting functionality
      if (settings.startPosition) {
        var originalLimit = settings.limit;
        settings.limit = function () {
          var plugin = this.data('ScrollToFixed');
          var viewportPositionTop = $(window).scrollTop();

          plugin.options.maxWidth = (viewportPositionTop > settings.startPosition) ? 0 : 1;

          if (originalLimit) {
            if (typeof originalLimit == 'number') {
              return originalLimit;
            }
            else {
              return originalLimit.apply(this, arguments);
            }
          }
        }
      }

      // Add animation class
      if (settings.addAnimationClass) {
        var originalPreFixed = settings.preFixed;

        settings.preFixed = function () {
          $(this).addClass('sticky--animation');

          if (originalPreFixed) {
            originalPreFixed.apply(this, arguments);
          }
        };
      }

      var originalFixed = settings.fixed;
      settings.fixed = function () {
        if (settings.addAnimationClass) {
          $(this).removeClass('sticky--animation');
        }
        if (settings.dontAddStyles) {
          $(this).removeAttr('style');
        }

        if (originalFixed) {
          originalFixed.apply(this, arguments);
        }
      };

      // Dont add styles
      if (settings.dontAddStyles) {
        var originalUnfixed = settings.unfixed;

        settings.unfixed = function () {
          if ($(this).css('position') == 'relative') {
            $(this).removeAttr('style');
          }

          if (originalUnfixed) {
            originalUnfixed.apply(this, arguments);
          }
        }
      }

      $(this.element).scrollToFixed(settings);
    },
  });

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, 'plugin_' + pluginName)) {
        $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
      }
    });
  };

})(jQuery, window, document);
