(function ($) {

  $.extend($.ui.dialog.prototype.options, {
    width: 'auto',
    maxHeight: '95%', // It's not work because maxHeight hardcoded in dialog.position.js. See xbase.js.
    modal: true,
    customOverlay: false,
    closeText: '✕',
    closeOnEscape: true,
    draggable: false,
    classes: {
      'ui-dialog': '',
      'ui-dialog-titlebar': '',
    },
  });

})(jQuery);
