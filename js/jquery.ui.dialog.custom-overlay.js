(function ($) {

  $.widget('ui.dialog', $.ui.dialog, {
    /**
     * Override _createOverlay().
     */
    _createOverlay: function () {
      // Show custom overlay
      if (this.options.modal && this.options.customOverlay) {
        Drupal.overlayShow('dialog', true);

        // Close dialog after click on overlay
        once('close', '.overlay').forEach(function (element) {
          $(element).on('click', function () {
            $('.ui-dialog-content').dialog('close');
          });
        });
      }
      else {
        this._superApply(arguments);
        $('body').addClass('ui-page-has-overlay');

        // Click on overlay will close dialog
        if (this.options.closeOnEscape) {
          var _this = this;
          this.overlay.on('click', function () {
            _this.close();
          });
        }

        // Callculate new position in case if ui-page-has-overlay class hide page scrollbar
        this._position();
      }
    },

    /**
     * Override _destroyOverlay().
     */
    _destroyOverlay: function () {
      // Hide custom overlay
      if (this.options.modal && this.options.customOverlay) {
        Drupal.overlayHide('dialog', true);
      }
      else {
        this._superApply(arguments);
        $('body').removeClass('ui-page-has-overlay');
      }
    },

    /**
     * Add public method for destroy overlay.
     */
    destroyOverlay: function () {
      this._destroyOverlay();
    },
  });

})(jQuery);
