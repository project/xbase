(function ($) {

  /**
   * window.matchMedia() helper. Support only pixels.
   */
  Drupal.matchMedia = function (expression) {
    var operatorMatches = expression.match(/^([<>=]{1,2})(.+)/);
    var media = '';

    if (operatorMatches) {
      var operator = operatorMatches[1];
      var breakpointName = operatorMatches[2];
      var breakpoint = (window.breakpoints && window.breakpoints[breakpointName])
        ? window.breakpoints[breakpointName]
        : parseInt(breakpointName, 10);

      if (operator == '<=') {
        media = 'max-width: ' + breakpoint + 'px';
      }
      else if (operator == '<') {
        media = 'max-width: ' + (breakpoint - 1) + 'px';
      }
      else if (operator == '>=') {
        media = 'min-width: ' + breakpoint + 'px';
      }
      else if (operator == '>') {
        media = 'min-width: ' + (breakpoint + 1) + 'px';
      }
    }
    else {
      media = expression;
    }

    return window.matchMedia('(' + media + ')').matches;
  };

  /**
   * Return CSS variable value.
   */
  Drupal.getCssVariable = function (variableName, element = document.documentElement, defaultValue = null, variableType = 'string') {
    if (element instanceof jQuery) {
      element = element.get(0);
    }
    var variableValue = getComputedStyle(element).getPropertyValue(variableName);

    if (variableValue !== '') {
      if (variableType == 'int') {
        return parseInt(variableValue, 10);
      }
      if (variableType == 'float') {
        return parseFloat(variableValue);
      }
    }

    return defaultValue;
  }

  /**
   * Return element offset relative to a specific parent.
   */
  Drupal.getElementRelativeOffset = function (element, parentElement) {
    const parentElementBoundingClientRect = parentElement.getBoundingClientRect()
    const elementBoundingClientRect = element.getBoundingClientRect();

    return {
      left: elementBoundingClientRect.left - parentElementBoundingClientRect.left,
      top: elementBoundingClientRect.top - parentElementBoundingClientRect.top,
    };
  }

  /**
   * Hide page scroll.
   */
  Drupal.disablePageScroll = function () {
    var $html = $('html');

    if ($html.css('overflow') != 'hidden') {
      $html.css({
        overflow: 'hidden',
        paddingRight: getScrollbarWidth(),
      });
    }
  };

  /**
   * Undisable body scroll.
   */
  Drupal.undisablePageScroll = function () {
    $('html').css({
      overflow: '',
      paddingRight: '',
    });
  };

  /**
   * Show overlay.
   */
  Drupal.overlayShow = function (overlayName, disablePageScroll) {
    var $overlay = $('.overlay');

    if ($overlay.hasClass('overlay--' + overlayName)) {
      return;
    }

    // Create new overlay
    if ($overlay.length == 0) {
      $overlay = $('<div class="overlay" />');
      $overlay.appendTo('body');
    }

    setTimeout(function () {
      if (disablePageScroll) {
        Drupal.disablePageScroll();
      }

      $overlay.addClass('overlay--visible');
      $overlay.addClass('overlay--' + overlayName);
      $('body').addClass('page--overlay-visible');
    }, 0);
  };

  /**
   * Show overlay.
   */
  Drupal.overlayHide = function (overlayName, undisablePageScroll) {
    var $overlay = $('.overlay');

    if ($overlay.length == 0) {
      return;
    }

    // Use timeout to prevent overlay blink between overlayHide() and overlayShow()
    setTimeout(function () {
      $overlay.removeClass('overlay--' + overlayName);
      if ($overlay[0].classList.length == 2) {
        $overlay.removeClass('overlay--visible');
        $('body').removeClass('page--overlay-visible');
      }

      if (undisablePageScroll) {
        Drupal.undisablePageScroll();
      }
    }, 10);
  };

  /**
   * Camelize string.
   */
  Drupal.camelize = function camalize(string, toLoverCase = false) {
    if (toLoverCase) {
      string = string.toLowerCase();
    }
    return string.replace(/[^a-zA-Z0-9]+(.)/g, (match, symbol) => symbol.toUpperCase());
  }

})(jQuery);
