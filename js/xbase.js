(function ($) {

  /**
   * DOM ready.
   */
  $(document).ready(function () {
    var htmlElement = document.querySelector('html');
    htmlElement.classList.remove('dom-not-ready');
    htmlElement.classList.add('dom-ready');

    // Integrate clickToggle plugin with custom overlay.
    $(this).on('clickToggleOpen clickToggleClose', function (event, settings) {
      if (settings.showOverlay) {
        // Show overlay
        if (event.type == 'clickToggleOpen') {
          Drupal.overlayShow('clicktoggle', settings.disableScroll);
        }
        // Hide overlay
        else {
          Drupal.overlayHide('clicktoggle', settings.disableScroll, settings.overlayHideTimeout);
        }
      }
    });
  });

  /**
   * Drupal javascript behaviors.
   */
  Drupal.behaviors.xbase = {
    attach: function attach(context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Clean phone in "tel:" links.
      // Example if href contains "+7 (123) 456-78-90", after clean href will equal "+71234567890".
      once('phone-link', 'a[href^="tel:"]').forEach(function (element) {
        var $a = $(element);
        var phone = $a.attr('href').match(/tel:(.+)/)[1];
        var cleanPhone = phone.replace(/[^\d+]/g, '');

        if (phone != cleanPhone) {
          $a.attr('href', 'tel:' + cleanPhone);
        }
      });

      // Open in Colorbox images with class "colorbox-content" and wrapped links.
      // Example: <a href="big.jpg"><img src="small.jpg" class="colorbox-content" /></a>
      if (Drupal.behaviors.initColorbox) {
        var $linksWithColorboxContent = $('a:not(.colorbox):has(img.colorbox-content)', context);
        if ($linksWithColorboxContent.length) {
          $linksWithColorboxContent.addClass('colorbox');
          Drupal.behaviors.initColorbox.attach($linksWithColorboxContent, settings);
        }
      }

      // Handle "Close" button in jQuery UI Dialog content.
      // Used setTimeout() to call code after jQuery UI Dialog _createButtons() method.
      setTimeout(function () {
        // Don't use "context" variable because dialog buttons are not in it
        once('close', '.ui-dialog-content-close').forEach(function (element) {
          $(element).on('click', function (event) {
            $(this).closest('.ui-dialog').find('.ui-dialog-content').dialog('close');
          });
        });
      });
    }
  };

})(jQuery);
