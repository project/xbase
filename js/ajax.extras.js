(function (Drupal) {

  /**
   * Override progress type "throbber".
   */
  Drupal.Ajax.prototype.setProgressIndicatorThrobber = function () {
    var element = this.element;

    element.classList.add('ajax-progress-animation');

    this.progress.object = {
      stopMonitoring: function () {
        element.classList.remove('ajax-progress-animation');
      }
    };
  };

  /**
   * Override progress type "fullscreen".
   */
  Drupal.Ajax.prototype.setProgressIndicatorFullscreen = function () {
    Drupal.overlayShow('ajax-loader', true);

    this.progress.object = {
      stopMonitoring: function () {
        Drupal.overlayHide('ajax-loader', true);
      }
    };
  };

})(Drupal);
