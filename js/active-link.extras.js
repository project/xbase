(function ($) {

  /**
   * Drupal javascript behaviors.
   */
  Drupal.behaviors.activeLinksExtras = {
    attach: function attach(context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }

      // Add class "is-active" to links
      var queryStringWithoutPager = removePagerFromQuery(location.search);
      context.querySelectorAll('a[href="' + location.pathname + queryStringWithoutPager + '"]').forEach(function (linkElement) {
        linkElement.classList.add('is-active');
      });

      // Add class is-active-trail" to links
      if (drupalSettings.path.currentQuery) {
        context.querySelectorAll('a[data-drupal-link-system-path="' + drupalSettings.path.currentPath + '"]:not([data-drupal-link-query])').forEach(function (linkElement) {
          linkElement.classList.add('is-active-trail');
        });
      }

      // Add class "is-active" and "is-active-trail" to parent li
      // @TODO Rewrite to vanilla js
      var $liContainingActiveLink = $('a.is-active').closest('li');
      if ($liContainingActiveLink.length) {
        $liContainingActiveLink
          .addClass('is-active')
          .parents('li').addClass('is-active-trail');
      }
    }
  };

  /**
   * Remove pager from query.
   */
  function removePagerFromQuery(query) {
    query = query.replace(/[?&]page=\d+/, '');
    if (query && query.startsWith('&')) {
      query = '?' + query.substring(1);
    }
    return query;
  }

})(jQuery);
