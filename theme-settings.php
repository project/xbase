<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter(): system_theme_settings.
 */
function xbase_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state): void {
  $form['advanced'] = [
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
  ];

  $form['advanced']['path_classes'] = [
    '#type' => 'checkbox',
    '#title' => t('Add current path based classes in body'),
    '#default_value' => theme_get_setting('path_classes'),
  ];

  $form['advanced']['breadcrumb_hide_one'] = [
    '#type' => 'checkbox',
    '#title' => t('Hide breadcrumb with one item'),
    '#default_value' => theme_get_setting('breadcrumb_hide_one'),
  ];

  $form['advanced']['breadcrumb_page_title'] = [
    '#type' => 'checkbox',
    '#title' => t('Add page title in breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_page_title'),
  ];

  $form['advanced']['breadcrumb_strip_tags'] = [
    '#type' => 'checkbox',
    '#title' => t('Strip tags in breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_strip_tags'),
  ];

  $form['advanced']['float_label'] = [
    '#type' => 'checkbox',
    '#title' => t('Float label'),
    '#default_value' => theme_get_setting('float_label'),
  ];
}
