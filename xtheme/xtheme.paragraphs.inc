<?php

use Drupal\Core\Render\Element;
use Drupal\Core\Template\AttributeHelper;
use Drupal\druhels\ArrayHelper;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Preprocess function for paragraph.html.twig.
 */
function xtheme_preprocess_paragraph(array &$vars): void {
  $vars['styles'] = $vars['elements']['#paragraph_styles'] ?? [];
  $vars['without_wrapper'] = isset($vars['styles']['without_wrapper']);
}

/**
 * Preprocess function for paragraph--paragraphs-block.html.twig.
 */
function xtheme_preprocess_paragraph__paragraphs_block(array &$vars): void {
  $paragraph = $vars['paragraph']; /** @var ParagraphInterface $slide_paragraph */

  // Set title field wrapper tag
  if (
    !empty($vars['content']['field_title'][0]) &&
    ($title_tag = $paragraph->get('field_title_tag')->value)
  ) {
    $vars['content']['field_title']['#wrapper_tag'] = $title_tag;
  }
}

/**
 * Preprocess function for paragraph--slide.html.twig.
 */
function xtheme_preprocess_paragraph__slide(array &$vars): void {
  $slide_paragraph = $vars['paragraph']; /** @var ParagraphInterface $slide_paragraph */
  $vars['attributes']['class'][] = $vars['html_class'] . '--' . $slide_paragraph->id();
}

/**
 * Preprocess function for paragraph--teasers.html.twig.
 */
function xtheme_preprocess_paragraph__teasers(array &$vars): void {
  $paragraph = $vars['paragraph']; /** @var ParagraphInterface $paragraph */
  $paragraph_behavior_settings = $paragraph->getAllBehaviorSettings();

  // Add css class to child teasers
  if ($selected_styles_ids = $paragraph_behavior_settings['style_selector']['styles'] ?? NULL) {
    $paragraph_type_entity = $paragraph->getParagraphType();
    $style_selector_plugin = $paragraph_type_entity->getBehaviorPlugin('style_selector');
    $styles = $style_selector_plugin->getConfiguration()['styles'];

    foreach ($selected_styles_ids as $selected_style_id) {
      if ($child_attributes = $styles[$selected_style_id]['child_attributes'] ?? NULL) {
        foreach (Element::children($vars['content']['field_paragraphs']) as $delta) {
          $vars['content']['field_paragraphs'][$delta]['#attributes'] = AttributeHelper::mergeCollections(
            $vars['content']['field_paragraphs'][$delta]['#attributes'] ?? [],
            ArrayHelper::formatArrayAsAttributes($child_attributes)
          );
        }
      }
    }
  }
}

/**
 * Preprocess function for paragraph--gallery.html.twig.
 */
function xtheme_preprocess_paragraph__gallery(array &$vars): void {
  $gallery_paragraph = $vars['paragraph']; /** @var ParagraphInterface $gallery_paragraph */

  // Change image style for child images
  if ($image_style_name = $gallery_paragraph->get('field_image_style')->target_id) {
    $vars['attributes']['class'][] = $vars['html_class'] .= '--' . str_replace('_', '-', $image_style_name);

    foreach (Element::children($vars['content']['field_images']) as $delta) {
      $vars['content']['field_images'][$delta]['#image_style'] = $image_style_name;
    }
  }
}
