<?php

namespace Drupal\xtheme;

use Drupal\Core\Security\TrustedCallbackInterface;

class XthemeTrustedCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'preRender',
    ];
  }

  public static function preRender(array $build): array {
    return $build;
  }

}
