/**
 * Don't merge objects using spread or Object.assign(), because they don't use deep merge.
 */

Drupal.carouselPresets = {};

// Slider with 1 slide per view
Drupal.carouselPresets.slider1 = {
  slidesPerView: 'auto',
  slidesPerGroupAuto: true,
  speed: 500,
  navigation: true,
  pagination: {
    enabled: true,
    clickable: true,
  },
  watchSlidesProgress: true,
};

// Slider with 2 slides per view
Drupal.carouselPresets.slider2 = {
  slidesPerView: 'auto',
  slidesPerGroupAuto: true,
  speed: 500,
  navigation: true,
  pagination: {
    enabled: true,
    clickable: true,
  },
  watchSlidesProgress: true,
};

// Slider with 3 slides per view
Drupal.carouselPresets.slider3 = {
  slidesPerView: 'auto',
  slidesPerGroupAuto: true,
  speed: 500,
  navigation: true,
  pagination: {
    enabled: true,
    clickable: true,
  },
  watchSlidesProgress: true,
};
