(function (Drupal) {

  // Create breakpoints
  window.breakpoints = {
    mobile: parseInt(Drupal.getCssVariable('--breakpoint-mobile'), 10),
    tablet: parseInt(Drupal.getCssVariable('--breakpoint-tablet'), 10),
    desktop: parseInt(Drupal.getCssVariable('--breakpoint-desktop'), 10),
  };

  /**
   * Drupal javascript behaviors.
   */
  Drupal.behaviors.xtheme = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }
    }
  };

})(Drupal);
