(function ($, Drupal) {

  /**
   * Media breakpoints.
   */
  Drupal.mediaBreakpoints = {
    'desktop': 980,
    'tablet': 640,
    'mobile': 480,
  };

  /**
   * Drupal javascript behaviors.
   */
  Drupal.behaviors.adaptive = {
    attach: function (context, settings) {
      if (context.tagName == 'SCRIPT') {
        return;
      }
    }
  };

  /**
   * DOM ready.
   */
  $(document).ready(function () {
    //
  });

})(jQuery, Drupal);
