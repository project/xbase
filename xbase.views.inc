<?php

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;

/**
 * Preprocess function for views-view.html.twig.
 */
function xbase_preprocess_views_view(array &$vars): void {
  $views = $vars['view']; /** @var ViewExecutable $views */

  $vars['html_class'] = xbase_get_views_html_class($views);

  // Views classes
  $views_classes = [
    $vars['html_class'],
    $vars['html_class'] . '--' . Html::getClass($vars['display_id']),
  ];

  // Add classes from parent element
  if (!empty($vars['view_array']['#attributes']['class'])) {
    $views_classes = array_merge($views_classes, array_diff($vars['view_array']['#attributes']['class'], ['views-element-container']));
  }

  if ($views->ajaxEnabled()) {
    /** @see \Drupal\views\Controller\ViewAjaxController::ajaxView() */
    $views_classes[] = 'js-view-dom-id-' . $vars['dom_id'];
  }

  $vars['attributes']['class'] = array_merge($views_classes, $vars['attributes']['class'] ?? []);

  // Content classes
  if (!isset($vars['content_attributes']['class'])) {
    $vars['content_attributes']['class'] = [];
  }
  array_unshift($vars['content_attributes']['class'], $vars['html_class'] . '__content');
}

/**
 * Preprocess function for views-view-table.html.twig.
 */
function xbase_preprocess_views_view_table(array &$vars): void {
  $views = $vars['view']; /** @var ViewExecutable $views */

  $vars['html_class'] = xbase_get_views_html_class($views);
  $vars['attributes']['class'][] =  $vars['html_class'] . '__table';
  $vars['attributes']['class'][] =  $vars['html_class'] . '__table--' . count($vars['header']);

  // Remove header th's "id" attribute
  foreach ($vars['header'] as &$info) {
    if (!empty($info['attributes']['id'])) {
      unset($info['attributes']['id']);
    }
  }

  // Remove td's "headers" attribute
  foreach ($vars['rows'] as $row_key => $row) {
    foreach ($row['columns'] as $column_key => $column) {
      $vars['rows'][$row_key]['columns'][$column_key]['attributes']->removeAttribute('headers');
    }
  }
}

/**
 * Preprocess function for views-view-list.html.twig.
 */
function xbase_preprocess_views_view_list(array &$vars): void {
  $views = $vars['view']; /** @var ViewExecutable $views */

  $vars['html_class'] = xbase_get_views_html_class($views);

  // Unset empty attributes, in order to remove list wrapper
  if (!$vars['attributes']) {
    unset($vars['attributes']);
  }
}

/**
 * Preprocess function for views-view-unformatted.html.twig.
 */
function xbase_preprocess_views_view_unformatted(array &$vars): void {
  $views = $vars['view']; /** @var ViewExecutable $views */

  $vars['html_class'] = xbase_get_views_html_class($views);
}

/**
 * Return Views html class.
 */
function xbase_get_views_html_class(ViewExecutable $views): string {
  return Html::getClass($views->storage->id()) . '-views';
}
