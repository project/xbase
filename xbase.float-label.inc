<?php

/**
 * Preprocess function for input.html.twig.
 */
function float_label_preprocess_input(array &$vars): void {
  if (theme_get_setting('float_label') && float_label_element_support_float_label($vars['element'])) {
    $vars['attributes']['class'][] = 'float-label__input';

    if (!isset($vars['attributes']['placeholder'])) {
      $vars['attributes']['placeholder'] = '';
    }
  }
}

/**
 * Preprocess function for form-element.html.twig.
 */
function float_label_preprocess_form_element(array &$vars): void {
  if (theme_get_setting('float_label') && float_label_element_support_float_label($vars['element'])) {
    $vars['attributes']['class'][] = 'float-label';
    $vars['label']['#attributes']['class'][] = 'float-label__label';
  }
}

/**
 * Return TRUE if element support float label.
 */
function float_label_element_support_float_label(array $element): bool {
  return
    in_array($element['#type'], ['textfield', 'textarea', 'number', 'email', 'password', 'search', 'tel']) &&
    !empty($element['#title']) &&
    $element['#title_display'] == 'before';
}
