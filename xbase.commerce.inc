<?php

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Markup;
use Drupal\druhels\CommerceHelper;

/**
 * Preprocess function for commerce-product.html.twig.
 */
function xbase_preprocess_commerce_product(array &$vars): void {
  $product = $vars['product_entity']; /** @var \Drupal\commerce_product\Entity\ProductInterface $product */

  $vars['product_id'] = $product->id();

  $vars['html_class'] = xbase_get_entity_html_class($product);
  $classes = [
    $vars['html_class'],
    $vars['html_class'] . '--' . Html::getClass($vars['elements']['#view_mode']),
  ];
  $vars['attributes']['class'] = array_merge($classes, $vars['attributes']['class'] ?? []);
}

/**
 * Preprocess function for block--commerce-cart.html.twig.
 */
function xbase_preprocess_block__commerce_cart(array &$vars): void {
  // Add css modificator to empty block
  if ($vars['elements']['content']['#count'] == 0) {
    $vars['attributes']['class'][] = $vars['html_class'] . '--empty';
  }
}

/**
 * Preprocess function for commerce-cart-block.html.twig.
 */
function xbase_preprocess_commerce_cart_block(array &$vars): void {
  // Add total price variable
  $vars['total'] = Markup::create(CommerceHelper::formatPrice(CommerceHelper::getCartsTotalPrice()));
}

/**
 * Preprocess function for commerce-order.html.twig.
 */
function xbase_preprocess_commerce_order(array &$vars): void {
  $order = $vars['order_entity']; /** @var OrderInterface $order */

  $vars['html_class'] = xbase_get_entity_html_class($order);

  $classes = [
    $vars['html_class'],
    $vars['html_class'] . '--' . Html::getClass($vars['elements']['#view_mode']),
  ];
  $vars['attributes']['class'] = array_merge($classes, $vars['attributes']['class'] ?? []);
}
