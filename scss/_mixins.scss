@use 'sass:list';
@use 'sass:map';

/**
 * Strip unit from number.
 *
 * Example:
 *   strip-unit(10px) // 10
 *   strip-unit(10%) // 10
 *   strip-unit(10) // 10
 */
@function strip-unit($number) {
  @if (type-of($number) == 'number' and not unitless($number)) {
    @return $number / ($number * 0 + 1);
  }
  @return $number;
}

/**
 * Convert string to length.
 */
@function str-to-length($value, $unit) {
  $units: (
    'px': 1px,
    'cm': 1cm,
    'mm': 1mm,
    '%': 1%,
    'ch': 1ch,
    'pc': 1pc,
    'in': 1in,
    'em': 1em,
    'rem': 1rem,
    'pt': 1pt,
    'ex': 1ex,
    'vw': 1vw,
    'vh': 1vh,
    'vmin': 1vmin,
    'vmax': 1vmax,
    'cqw': 1cqw,
  );

  @if not list.index(map.keys($units), $unit) {
    @error 'Invalid unit `#{$unit}`.';
  }

  @return $value * map.get($units, $unit);
}

/**
 * Convert string to number.
 *
 * Example:
 *   str-to-number('20px') // 20px
 *   str-to-number('10vw') // 10vw
 */
@function str-to-number($value) {
  @if (type-of($value) == 'number') {
    @return $value;
  }
  @else if (type-of($value) != 'string') {
    @error 'Value for `str-to-number` should be a number or a string.';
  }

  $result: 0;
  $digits: 0;
  $minus: str-slice($value, 1, 1) == '-';
  $numbers: ('0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9);

  @for $i from if($minus, 2, 1) through str-length($value) {
    $character: str-slice($value, $i, $i);

    @if (not (index(map-keys($numbers), $character) or $character == '.')) {
      @return str-to-length(if($minus, -$result, $result), str-slice($value, $i))
    }

    @if ($character == '.') {
      $digits: 1;
    }
    @else if ($digits == 0) {
      $result: $result * 10 + map_get($numbers, $character);
    }
    @else {
      $digits: $digits * 10;
      $result: $result + map_get($numbers, $character) / $digits;
    }
  }

  @return if($minus, -$result, $result);
}

/**
 * Convert pixels to relative units.
 *
 * Example:
 *   px-to-relative(480px, 960px, '%') // 50%
 *   px-to-relative(240px, 960px, 'vw') // 25vw
 */
@function px-to-relative($target-size, $outer-size, $unit) {
  $result: (100 / strip-unit($outer-size)) * strip-unit($target-size);
  @return str-to-number($result + $unit);
}

/**
 * Convert px to %.
 *
 * Example:
 *   px-to-percent(480px, 960px) // 50%
 *   px-to-percent(240px, 960px) // 25%
 */
@function px-to-percent($target-size, $outer-size) {
  @return px-to-relative($target-size, $outer-size, '%');
}

/**
 * Convert px to vw.
 *
 * Example:
 *   px-to-vw(480px, 960px) // 50vw
 *   px-to-vw(240px, 960px) // 25vw
 */
@function px-to-vw($target-size, $outer-size) {
  @return px-to-relative($target-size, $outer-size, 'vw');
}

/**
 * Convert % to px.
 *
 * Example:
 *   percent-to-px(25%, 960px) // 240px
 *   percent-to-px(50%, 1200px) // 600px
 */
@function percent-to-px($percent, $outer-size) {
  $percent: strip-unit($percent);
  $outer-size: strip-unit($outer-size);
  @return (($percent / 100) * $outer-size) * 1px;
}

/**
 * Convert hex to rgb.
 *
 * Example:
 *   hex-to-rgb(#000000) // 0, 0, 0
 *   hex-to-rgb(#FFFFFF) // 255, 255, 255
 */
@function hex-to-rgb($color) {
  @return red($color) + ', ' + green($color) + ', ' + blue($color);
}

/**
 * Shortcut for minmax(0, 1fr).
 *
 * Example:
 *   fr(1) // minmax(0, 1fr)
 *   fr(2) // minmax(0, 2fr)
 */
@function fr($number) {
  @return minmax(0, #{strip-unit($number)}fr);
}

/**
 * String replace.
 *
 * Example:
 *   str-replace('hello world', 'world', 'john') // 'hello john'
 */
@function str-replace($string, $search, $replace) {
  $index: str-index($string, $search);

  @if $index {
    @return
      str-slice($string, 1, $index - 1) +
      $replace +
      str-replace(str-slice($string, $index + str-length($search)), $search, $replace);
  }

  @return $string;
}

/**
 * Trim string.
 *
 * Example:
 *   str-trim(' foo ') // 'foo'
 *   str-trim('bar ') // 'bar'
 */
@function str-trim($string) {
  @if (str-slice($string, 1, 1) == ' ') {
    @return str-trim(str-slice($string, 2));
  }
  @else if (str-slice($string, str-length($string), -1) == ' ') {
    @return str-trim(str-slice($string, 1, -2));
  }
  @else {
    @return $string;
  }
}

/* Return one column width */
@function column-width($columns-count: 'var(--grid-columns)', $columns-gap: 'var(--grid-gap)') {
  @return calc(
    (100% - #{$columns-gap} * (#{$columns-count} - 1)) / #{$columns-count}
  );
}

/**
 * Return responsive font size.
 *
 * Example:
 *   responsive-font-size(40px, 20px, $min-width: 320px)
 */
@function responsive-font-size($max-font-size, $min-font-size, $max-width: $page-content-max-width + $page-padding * 2, $min-width: $page-min-width) {
  @return clamp(
    $min-font-size,
    calc(
      $min-font-size +
      strip-unit($max-font-size - $min-font-size) *
      ((100vw - $min-width) / strip-unit($max-width - $min-width))
    ),
    $max-font-size
  );
}

/* Clearfix */
@mixin clearfix() {
  &::after {
    content: '';
    display: table;
    clear: both;
  }
}

/* Remove clearfix */
@mixin clearfix-reset() {
  &::after {
    display: none;
  }
}

/* Page padding */
@mixin page-padding() {
  padding-left: var(--page-padding);
  padding-right: var(--page-padding);
}

/* Compensate horizontal page padding */
@mixin negative-page-padding() {
  margin-left: calc(var(--page-padding) * -1);
  margin-right: calc(var(--page-padding) * -1);
}

/* Layout row */
@mixin layout-row($inner-selector: ' > *', $add-page-padding: true) {
  @if ($add-page-padding) {
    @include page-padding();
  }
  @include inner-selector($inner-selector) {
    @include layout-cell();
  };
}

/* Layout cell */
@mixin layout-cell() {
  max-width: var(--page-content-max-width);
  margin-left: auto;
  margin-right: auto;
}

/* Layout cell reset */
@mixin layout-cell-reset() {
  max-width: none;
  margin-left: 0;
  margin-right: 0;
}

/* Layout row reset */
@mixin layout-row-reset($inner-selector: ' > *') {
  padding-left: 0;
  padding-right: 0;

  #{$inner-selector}  {
    @include layout-cell-reset();
  }
}

/* Full width element */
@mixin full-width() {
  $margin: calc(50% - var(--page-current-width) / 2);
  margin-left: $margin;
  margin-right: $margin;
}

/* Dotted underline */
@mixin dotted-underline($color, $dot-width: 1px, $dot-height: 1px, $separator-width: 1px) {
  background-image: linear-gradient(to right, $color $dot-width, transparent $dot-width);
  background-size: ($separator-width + $dot-width) $dot-height;
  background-repeat: repeat-x;
  background-position: 0 bottom;
}

/* Dotted underline reset*/
@mixin dotted-underline-reset() {
  background: none;
}

/* Triangle */
@mixin triangle($direction, $width, $height, $color, $pseudo-selector: after) {
  &::#{$pseudo-selector} {
    content: '';
    display: block;
    width: 0;
    height: 0;
    border-style: solid;
    border-color: transparent;

    @if ($direction == 'top' or $direction == 'up') {
      border-width: 0 ($width/2) $height ($width/2);
    }
    @else if ($direction == 'bottom' or $direction == 'down') {
      border-width: $height ($width/2) 0 ($width/2);
    }
    @else if ($direction == 'left') {
      border-width: ($height/2) $width ($height/2) 0;
    }
    @else if ($direction == 'right') {
      border-width: ($height/2) 0 ($height/2) $width;
    }
    @else {
      @error 'Wrong direction.';
    }

    @include triangle-color($direction, $color);

    @content;
  }
}

/* Triangle color */
@mixin triangle-color($direction, $color) {
  @if ($direction == 'top' or $direction == 'up') {
    border-bottom-color: $color;
  }
  @else if ($direction == 'bottom' or $direction == 'down') {
    border-top-color: $color;
  }
  @else if ($direction == 'left') {
    border-right-color: $color;
  }
  @else if ($direction == 'right') {
    border-left-color: $color;
  }
  @else {
    @error 'Wrong direction.';
  }
}

/* Outer lines */
@mixin outer-lines($color: black, $margin: 10px, $top: -1px) {
  overflow: hidden;
  position: relative;
  text-align: center;
  white-space: nowrap;

  &:before,
  &:after {
    content: '';
    display: inline-block;
    position: relative;
    top: $top;
    width: 50%;
    height: 1px;
    vertical-align: middle;
    background: $color;
  }
  &:before {
    left: (0 - $margin);
    margin-left: -50%;
  }
  &:after {
    left: $margin;
    margin-right: -50%;
  }
  > span {
    display: inline-block;
    vertical-align: middle;
    white-space: normal;
    max-width: 85%;
  }
}

/* Outer lines reset */
@mixin outer-lines-reset() {
  overflow: visible;
  text-align: left;
  white-space: normal;

  &:before,
  &:after {
    display: none;
  }
  > span {
    display: inline;
    max-width: none;
  }
}

/* Remove extreme property */
@mixin remove-extreme-property($side, $property) {
  @if ($side == 'top') {
    &:first-child {
      #{$property}-top: 0;
    }
  }
  @else if ($side == 'bottom') {
    &:last-child {
      #{$property}-bottom: 0;
    }
  }
  @else if ($side == 'left') {
    &:first-child {
      #{$property}-left: 0;
    }
  }
  @else if ($side == 'right') {
    &:last-child {
      #{$property}-right: 0;
    }
  }
}

/* Remove first and last margins */
@mixin remove-extreme-margin($sides: 'bottom') {
  @each $side in $sides {
    @include remove-extreme-property($side, 'margin');
  }
}

/* Remove first and list p margins */
@mixin remove-extreme-p-margins($nested: false) {
  @if (not $nested) {
    & > p {
      @include remove-extreme-margin(top bottom);
    }
  }
  @else {
    p {
      @include remove-extreme-margin(top bottom);
    }
  }
}

/* Build inner selector */
@mixin inner-selector($selector) {
  @if (str_slice($selector, 1, 1) == '&') {
    @at-root &#{str_slice($selector, 2)} {
      @content;
    }
  }
  @else {
    #{$selector} {
      @content;
    }
  }
}

/* Full width background */
@mixin full-width-background($pseudo-element: 'before') {
  position: relative;

  &::#{$pseudo-element} {
    content: '';
    position: absolute;
    top: 0;
    left: calc(50% - var(--correct-100vw) / 2);
    right: calc(50% - var(--correct-100vw) / 2);
    max-width: calc(var(--correct-100vw) - 0.01px); // Fix Chrome 122
    height: 100%;
    z-index: -1;
    @content;
  }
}

/* Visually hidden */
@mixin visually-hidden() {
  position: absolute !important;
  overflow: hidden;
  clip: rect(1px, 1px, 1px, 1px);
  width: 1px;
  height: 1px;
  word-wrap: normal;
}

/* Reset visually-hidden styles */
@mixin visually-hidden-reset() {
  position: initial !important;
  overflow: initial;
  clip: initial;
  width: initial;
  height: initial;
  word-wrap: initial;
}

/* Hamburger icon */
@mixin hamburger-icon($lines-count: 3, $line-height: 2px, $lines-gap: 5px, $line-color: black) {
  $gradients: '';
  $line-height: strip-unit($line-height);
  $lines-gap: strip-unit($lines-gap);
  $icon-height: ($line-height * $lines-count) + ($lines-gap * ($lines-count - 1));

  @for $i from 0 through ($lines-count - 1) {
    $line-start: ($line-height + $lines-gap) * $i;
    $line-end: $line-start + $line-height;

    // Line
    $gradients: $gradients + ', ' + $line-color + ' ' + percentage($line-start / $icon-height) + ', ' + $line-color + ' ' + percentage($line-end / $icon-height);

    // Transparency
    @if $i < ($lines-count - 1) {
      $next-line-start: ($line-height + $lines-gap) * ($i + 1);
      $gradients: $gradients + ', transparent ' + percentage($line-end / $icon-height) + ', transparent ' + percentage($next-line-start / $icon-height);
    }
  }

  background-image: linear-gradient(to bottom #{$gradients});
  background-position: center;
  background-repeat: no-repeat;
}

/* Overlay */
@mixin overlay() {
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: var(--z-index-overlay);
  background: var(--overlay-background);
}

/* Transition with multiple properties */
@mixin transition-multiple($properties, $duration: 0.2s, $delay: null) {
  transition-property: join($properties, (), 'comma');
  transition-duration: $duration;

  @if ($delay) {
    transition-delay: $delay;
  }
}

/* Lines using gradient */
@mixin lines-using-gradient($direction, $lines) {
  $gradient: '';
  $last-line-end: 0;

  @each $key, $line in $lines {
    $line-color: nth($line, 1);
    $line-start: nth($line, 2);
    $line-end: nth($line, 3);

    @if ($line-start != $last-line-end) {
      $gradient: $gradient + ', transparent #{$last-line-end}, transparent #{$line-start}';
    }

    $gradient: $gradient + ', #{$line-color} #{$line-start}, #{$line-color} #{$line-end}';

    $last-line-end: $line-end;
  }

  @if ($last-line-end != 100%) {
    $gradient: $gradient + ', transparent #{$last-line-end}';
  }

  background-image: linear-gradient($direction#{$gradient});
}

/* Hide scrollbar but non disable scrolling */
@mixin hide-scrollbar() {
  // Firefox
  scrollbar-width: none;

  // Webkit
  &::-webkit-scrollbar {
    display: none;
  }
}

/* Hide element */
@mixin hide($transition-duration: 0.2s, $transition-delay: null) {
  @include transition-multiple(visibility opacity, $transition-duration, $transition-delay);
  visibility: hidden;
  opacity: 0;
}

/* Show element */
@mixin show() {
  visibility: visible;
  opacity: 1;
}

/* Left icon */
@mixin with-left-icon($icon-selector, $icon-width, $icon-height, $icon-margin) {
  --element-icon-width: #{$icon-width};
  --element-icon-height: #{$icon-height};
  --element-icon-margin: #{$icon-margin};
  padding-left: calc(var(--element-icon-width) + var(--element-icon-margin));

  @include inner-selector($icon-selector) {
    display: inline-block;
    width: var(--element-icon-width);
    height: var(--element-icon-height);
    margin-right: var(--element-icon-margin);
    margin-left: calc((var(--element-icon-width) + var(--element-icon-margin)) * -1);
  };
}

/* Hide siblings */
@mixin hide-siblings($start-index) {
  &:nth-child(n+#{$start-index}) {
    display: none;
  }
}

/* Keep maximum first elements */
@mixin keep-first($count) {
  @include hide-siblings($count + 1);
}

/* Adaptive version keep-first() mixin */
@mixin keep-first-adaptive($map) {
  @each $media-query, $count in $map {
    @if ($media-query == 'default') {
      @include keep-first($count);
    }
    @else {
      @include media($media-query) {
        @include keep-first($count);
      }
    }
  }
}

/* Add label colon */
@mixin label-colon() {
  &::after {
    content: ':';
  }
}

/* Text inputs */
@mixin text-inputs() {
  input[type="text"],
  input[type="number"],
  input[type="url"],
  input[type="email"],
  input[type="password"],
  input[type="date"],
  input[type="time"],
  input[type="tel"],
  textarea,
  select {
    @content;
  }
}

/* Background image with custom color */
@mixin background-image-with-custom-color($url, $color: null, $size: null, $position: center, $repeat: no-repeat) {
  -webkit-mask-image: url($url);
  mask-image: url($url);

  @if ($color) {
    background-color: $color;
  }
  @if ($position) {
    -webkit-mask-position: $position;
    mask-position: $position;
  }
  @if ($repeat) {
    -webkit-mask-repeat: $repeat;
    mask-repeat: $repeat;
  }
  @if ($size) {
    -webkit-mask-size: $size;
    mask-size: $size;
  }
}

/* Page grid system */
@mixin page-grid() {
  display: grid;
  grid-template-columns: repeat(var(--grid-columns), 1fr);
  column-gap: var(--grid-gap);
  grid-auto-rows: max-content;
}

/* Grid auto columns */
@mixin grid-auto-columns($column-min-width) {
  // https://habr.com/ru/articles/499088/
  grid-template-columns: repeat(auto-fit, minmax(min($column-min-width, 100%), 1fr));
}

/* Pixelated image */
@mixin pixelated-image() {
  image-rendering: pixelated;
}

/* Centering absolute element */
@mixin absolute-center() {
  position: absolute;
  inset: 0;
  margin: auto;
}

/* Text gradient */
@mixin text-gradient($gradient) {
  background: $gradient;
  -webkit-background-clip: text;
  background-clip: text;
  -webkit-text-fill-color: transparent;
}

/* Text gradient reset */
@mixin text-gradient-reset() {
  background: initial;
  -webkit-background-clip: initial;
  background-clip: initial;
  -webkit-text-fill-color: initial;
}

/* Carousel content styles */
@mixin carousel-content-styles() {
  &:not(.swiper-initialized),
  & .swiper-wrapper {
    @content;
  }
}

/* Hide input[type="number"] arrows */
@mixin hide-input-number-arrows() {
  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
}
